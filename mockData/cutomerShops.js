const customerShops = [
  {
    id: 1, 
    name: "Stemofel",
    location: "Eldoret", 
    operationalRegion: "Juniorate",
    orderItems: [
      {
        id: 1, name: "Bulb", qty: 23, price: 100,
      },
      {
        id: 2, name: "Extension", qty: 4, price: 450,
      },
    ],
  },
  {
    id: 2, 
    name: "Kwa Nyaga",
    location: "Nairobi", 
    operationalRegion: "Githurai",
    orderItems: [
      {
        id: 3, name: "Choma", qty: 1, price: 80,
      },
    ],
  },
];

module.exports = customerShops;