const express = require("express");
const WebSocket = require("ws");
const customerShops = require("./mockData/cutomerShops");

const app = express();

const PORT = process.env.PORT || 3001;

// HTTP ROUTES
app.get("/", (req, res) => {
  res.send("Hey There!");
});

const server = app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});


// WEBSOCKETs (AS A SERVER)
function heartbeat() {
  this.isAlive = true;
  // this.isAlive = false;
  // console.log("pong received from client");
}

// const wss = new WebSocket.Server({ server, path: "/" });
const wss1 = new WebSocket.Server({ noServer: true });
const wss2 = new WebSocket.Server({ noServer: true });

function noop() { }

// ws server 1
wss1.on("connection", (ws, request) => {
  console.log("Client Connected");

  // setInterval(() => {
  //   ws.send(Math.random().toString());
  // }, 1000);

  ws.isAlive = true;
  ws.on("pong", heartbeat);

  ws.send("Connected to WS server!!");

  ws.on("message", (data) => {
    console.log(data);
    ws.send(`Server received msg -> ${data}`);
  });

  ws.on("close", () => {
    console.log("Client disconnected");
  });
});

// TODO: make the interval applicable to all wss instance, not only wss1
const interval = setInterval(() => {
  wss1.clients.forEach(ws => {
    // console.log(ws.isAlive);
    if (ws.isAlive === false) {
      return ws.terminate();
    }

    ws.isAlive = false;
    ws.ping(noop);
  })
}, 3000);

wss1.on("close", () => {
  clearInterval(interval);
  console.log("Interval Cleared (on wss close)");
});


// ws server 2
wss2.on("connection", ws => {
  console.log("Client connected to ws server 2");
  // ws.send("Connected to WS server 2!!".toUpperCase());

  ws.on("close", () => {
    console.log("Client disconnected from ws server 2");
  });

  ws.on("message", (data) => {
    console.log(data);
  });

  // mimic sending order to the ws client
  // TODO: have a better data structure
  setTimeout(() => {
    let newOrder = {
      id: 5,
      customer: {
        id: 23,
        name: "Adoshe",
        contact: "87687",
      },
      customerShops: customerShops,
      deliveryInfo: {
        destination: "Kisumu",
        deliveryMode: "Motorbike",
        maxAllowableDistance: 600,
        distanceFixed: 2,
        distanceInterval: 2,
        amountFixed: 50,
        amountInterval: 30
      }
    }
    ws.send(JSON.stringify(newOrder));
  }, 7500);
});


// handling handshakes
server.on("upgrade", (request, socket, head) => {
  const requestUrl = new URL(request.url, `http://127.0.0.1:${PORT}`);
  const pathname = requestUrl.pathname;

  console.log(pathname);

  if (pathname === "/") {
    wss1.handleUpgrade(request, socket, head, (ws) => {
      wss1.emit("connection", ws, request);
    });
  } else if (pathname === "/newOrder") {
    wss2.handleUpgrade(request, socket, head, (ws) => {
      wss2.emit("connection", ws, request);
    });
  } else {
    socket.destroy();
  }
});
